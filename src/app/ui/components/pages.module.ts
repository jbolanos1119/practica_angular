import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PRIMENG_MODULES } from 'src/app/shared/primeNg/primeNg';

// Componentes
import { HeaderComponent } from './layouts/header/header.component';
import { FooterComponent } from './layouts/footer/footer.component';


@NgModule({
  declarations: [
     HeaderComponent,
     FooterComponent
    ],
  imports: [
    CommonModule,
    ...PRIMENG_MODULES
  ],
  exports: [
    HeaderComponent,
     FooterComponent,
    ...PRIMENG_MODULES
   ]
})
export class PagesModule { }
